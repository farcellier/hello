#!/usr/bin/env bash

readonly SCRIPT_DIR=$( cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd );
readonly DIST_DIR="${SCRIPT_DIR}/../dist"

function main
{
  set -o errexit
  set -o pipefail
  set -o nounset
  set -o errtrace

  mkdir -p "${DIST_DIR}"
  npm install
  npm test
  now=$(date +"%Y%m%d_%H%M")
  identifiant="hello@${now}"
  echo "${identifiant}" > "${SCRIPT_DIR}/../VERSION"
  zip --symlinks -r "${DIST_DIR}/${identifiant}.zip" . -x "/.git*" "*.DS_Store*" "/.idea*" "/scripts*" "/dist*"
  cp "${DIST_DIR}/${identifiant}.zip" "${DIST_DIR}/hello@latest.zip"
  aws s3 cp "${DIST_DIR}/hello@latest.zip" "s3://615535196854-hello/hello@latest.zip"
}

function error_exit
{
  (>&2 echo "$1")
  exit 1
}

main "$@"